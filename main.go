package main

import (
	"hoolog-exporter/cmd"
)

func main() {
	cmd.Exec()
}

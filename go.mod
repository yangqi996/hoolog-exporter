module hoolog-exporter

go 1.16

require (
	github.com/olivere/elastic/v7 v7.0.31
	github.com/prometheus/client_golang v1.12.1
	github.com/spf13/viper v1.10.1
)

package cmd

import (
	"fmt"
	"hoolog-exporter/prom"
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func Exec() {
	http.Handle("/metrics", prom.HandlerWithGauge(promhttp.Handler(), prom.SlowlogGauge))
	http.HandleFunc("/errors/lastweek", helloWorld)

	prometheus.MustRegister(prom.SlowlogGauge)

	log.Fatal(http.ListenAndServe(":8888", nil))

}

func helloWorld(rw http.ResponseWriter, req *http.Request) {
	for i := 1; i < 8; i++ {
		fmt.Println(i)
	}
	fmt.Fprint(rw, "Hello world")
}

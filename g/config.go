package g

import (
	"fmt"

	"github.com/spf13/viper"
)

func Config() {

	v := viper.New()
	v.SetConfigFile("./conf/config.yaml")

	//	var indexconfig []g.Index
	//	v.UnmarshalKey("index", &indexconfig)
	//	for _, h := range indexconfig {
	//		fmt.Printf(h.Name, h.Filed)
	//	}

	err := v.ReadInConfig() // 查找并读取配置文件
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	Host = fmt.Sprintf("%v", v.Get("host"))

	indexs := v.Get("indexs")
	indexsmap := indexs.([]interface{})

	for _, vmap := range indexsmap {
		eachElement := vmap.(map[interface{}]interface{})
		name := fmt.Sprintf("%v", eachElement["name"])
		filed := fmt.Sprintf("%v", eachElement["filed"])
		Indexs = append(Indexs, Index{Name: name, Filed: filed})
	}
}

package prom

import (
	"fmt"
	"hoolog-exporter/eslog"
	"hoolog-exporter/g"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

var SlowlogGauge = prometheus.NewGaugeVec(
	prometheus.GaugeOpts{
		Name: "hoolog_slowlog_day_count",
		Help: "hoolog opensearch slowlog total by day",
	}, []string{"indexname", "day", "dbtype"})

var SlowlogCounter = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "hoolog_slowlog_total",
		Help: "Number of hard-disk errors.",
	},
	[]string{"indexname", "dbtype"},
)

func HandlerWithGauge(handler http.Handler, gauge *prometheus.GaugeVec) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		status := http.StatusOK
		for _, index := range g.Indexs {
			timeday := time.Now().Format("2006-01-02")
			indexname := fmt.Sprintf("%s-%s", index.Name, timeday)
			count := eslog.IndexSearchCount(indexname, index.Filed, "error")
			SlowlogGauge.With(prometheus.Labels{"indexname": index.Name, "day": timeday, "dbtype": "mysql"}).Set(count)
		}

		if req.Method == http.MethodGet {
			handler.ServeHTTP(w, req)
			return
		}
		status = http.StatusBadRequest

		w.WriteHeader(status)
	})
}

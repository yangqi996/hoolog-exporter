package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"reflect"
	"time"

	"github.com/olivere/elastic/v7"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	client *elastic.Client
	host   = "http://192.168.1.9:9200"

	hdFailures = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "hd_errors_total",
			Help: "Number of hard-disk errors.",
		},
		[]string{"device"},
	)
)

type Employee struct {
	FirstName string   `json:"first_name"`
	LastName  string   `json:"last_name"`
	Age       int      `json:"age"`
	About     string   `json:"about"`
	Interests []string `json:"interests"`
}

//初始化
func init() {
	var err error
	client, err = elastic.NewClient(elastic.SetSniff(false), elastic.SetURL(host))
	if err != nil {
		panic(err)
	}
	_, _, err = client.Ping(host).Do(context.Background())
	if err != nil {
		panic(err)
	}

	_, err = client.ElasticsearchVersion(host)
	if err != nil {
		panic(err)
	}

	prometheus.MustRegister(hdFailures)
}

func printEmployee(res *elastic.SearchResult, err error) {
	if err != nil {
		print(err.Error())
		return
	}
	var typ Employee
	for _, item := range res.Each(reflect.TypeOf(typ)) { //从搜索结果中取数据的方法
		t := item.(Employee)
		fmt.Printf("%#v\n", t)
	}
}

//创建
func create() {
	timeday := time.Now().Format("2006-01-02")
	indexname := fmt.Sprintf("test-%s", timeday)
	e1 := Employee{"Jane", "Smith", 32, "I like to collect rock albums", []string{"music"}}
	put1, err := client.Index().
		Index(indexname).
		Type("employee").
		Id("1").
		BodyJson(e1).
		Do(context.Background())
	if err != nil {
		panic(err)
	}
	fmt.Printf("Indexed tweet %s to index s%s, type %s\n", put1.Id, put1.Index, put1.Type)

	//使用字符串
	e2 := `{"first_name":"John","last_name":"Smith","age":25,"about":"I love to go rock climbing","interests":["sports","music"]}`
	put2, err := client.Index().
		Index(indexname).
		Type("employee").
		Id("2").
		BodyJson(e2).
		Do(context.Background())
	if err != nil {
		panic(err)
	}
	fmt.Printf("Indexed tweet %s to index s%s, type %s\n", put2.Id, put2.Index, put2.Type)

	e3 := `{"first_name":"Douglas","last_name":"Fir","age":35,"about":"I like to build cabinets","interests":["forestry"]}`
	put3, err := client.Index().
		Index(indexname).
		Type("employee").
		Id("3").
		BodyJson(e3).
		Do(context.Background())
	if err != nil {
		panic(err)
	}
	fmt.Printf("Indexed tweet %s to index s%s, type %s\n", put3.Id, put3.Index, put3.Type)
}

////搜索
func query() float64 {
	timeday := time.Now().Format("2006-01-02")
	indexname := fmt.Sprintf("test-%s", timeday)
	var res *elastic.SearchResult
	var err error
	//取所有
	res, err = client.Search(indexname).Type("employee").Do(context.Background())
	// printEmployee(res, err)

	//短语搜索 搜索about字段中有 rock climbing
	matchPhraseQuery := elastic.NewMatchPhraseQuery("about", "rock climbing")
	res, err = client.Search(indexname).Type("employee").Query(matchPhraseQuery).Do(context.Background())
	printEmployee(res, err)

	fmt.Println(res.TotalHits())

	return float64(res.TotalHits())

}

func recordMetrics() {
	go func() {
		for {
			hdFailures.With(prometheus.Labels{"device": "/dev/sda"}).Add(1)
			time.Sleep(10 * time.Second)
		}
	}()
}

func main() {
	create()
	count := query()
	fmt.Println(count)

	recordMetrics()
	http.Handle("/metrics", promhttp.Handler())

	log.Fatal(http.ListenAndServe(":8888", nil))
}

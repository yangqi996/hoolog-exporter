package eslog

import (
	"fmt"
	"hoolog-exporter/g"
	"net/http"
	"time"
)

type CountValue struct {
	day   string
	count float64
}

func ErrorCount(rw http.ResponseWriter, req *http.Request) {
	//result := make(map[string]map[string][]CountValue)
	result := make(map[string][]CountValue)
	nowday := time.Now()
	// nowday := time.Now().Format("2006-01-02")
	for i := 1; i < 8; i++ {
		daytime := nowday.AddDate(0, 0, -i).Format("2006-01-02")
		for _, index := range g.Indexs {
			indexname := fmt.Sprintf("%s-%s", index.Name, daytime)
			c := IndexSearchCount(indexname, index.Filed, "error")
			fmt.Println(c)
			result[indexname] = append(result[indexname], CountValue{day: daytime, count: c})
		}
	}
}

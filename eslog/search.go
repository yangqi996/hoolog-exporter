package eslog

import (
	"context"
	"fmt"
	"hoolog-exporter/g"
	"reflect"

	"github.com/olivere/elastic/v7"
)

var (
	client *elastic.Client
)

type Employee struct {
	FirstName string   `json:"first_name"`
	LastName  string   `json:"last_name"`
	Age       int      `json:"age"`
	About     string   `json:"about"`
	Interests []string `json:"interests"`
}

//初始化
func init() {
	//g.Host = "http://192.168.1.9:9200"
	g.Config()
	var err error
	client, err = elastic.NewClient(elastic.SetSniff(false), elastic.SetURL(g.Host))
	if err != nil {
		panic(err)
	}
	_, _, err = client.Ping(g.Host).Do(context.Background())
	if err != nil {
		panic(err)
	}

	_, err = client.ElasticsearchVersion(g.Host)
	if err != nil {
		panic(err)
	}

}

func printEmployee(res *elastic.SearchResult, err error) {
	if err != nil {
		print(err.Error())
		return
	}
	var typ Employee
	for _, item := range res.Each(reflect.TypeOf(typ)) { //从搜索结果中取数据的方法
		t := item.(Employee)
		fmt.Printf("%#v\n", t)
	}
}

////搜索
func IndexSearchCount(indexname string, filed string, content string) float64 {
	var res *elastic.SearchResult
	var err error
	matchPhraseQuery := elastic.NewMatchPhraseQuery(filed, content)
	res, err = client.Search(indexname).Type(indexname).Query(matchPhraseQuery).Do(context.Background())
	printEmployee(res, err)

	return float64(res.TotalHits())
}

func IndexSearch(indexname string, filed string, content string) *elastic.SearchResult {
	var res *elastic.SearchResult
	var err error
	matchPhraseQuery := elastic.NewMatchPhraseQuery(filed, content)
	res, err = client.Search(indexname).Type(indexname).Query(matchPhraseQuery).Do(context.Background())
	printEmployee(res, err)

	return res
}
